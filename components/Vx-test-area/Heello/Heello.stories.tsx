import React from "react";
import Heello from "./Heello.component";

import { action } from "@storybook/addon-actions";
import { withKnobs, object } from "@storybook/addon-knobs";

// export default { title: "abas" };

export default {
  component: Heello,
  title: "Heello",
  decorators: [withKnobs],
  excludeStories: /.*Data$/,
};

export const heelloWithKnobs = () => (
  <Heello task={object("task", "msdsadas")} />
);

// import { actions } from "@storybook/addon-actions";
// import Button from "./button";

// export default {
//   title: "Button",
//   component: Button,
// };

// // This will lead to { onClick: action('onClick'), ... }
// const eventsFromNames = actions("onClick", "onMouseOver");

// // This will lead to { onClick: action('clicked'), ... }
// const eventsFromObject = actions({
//   onClick: "clicked",
//   onMouseOver: "hovered",
// });

// export const first = () => <Button {...eventsFromNames}>Hello World!</Button>;

// export const second = () => <Button {...eventsFromObject}>Hello World!</Button>;
