import React from "react";
import Second from "./Second.component";
import { withA11y } from "@storybook/addon-a11y";

export default { title: "second", decorators: [withA11y] };

export const sec = () => <Second />;
