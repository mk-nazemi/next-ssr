import { NestFactory } from "@nestjs/core";
import {
  NestExpressApplication,
  ExpressAdapter,
} from "@nestjs/platform-express";
import { NextModule } from "@nestpress/next";

// --------------------------------------------------------

import cookieParser from "cookie-parser";
// import helmet from "helmet";
// import csurf from "csurf";
// import rateLimit from "express-rate-limit";

// --------------------------------------------------------

import { AppModule } from "./app.module";

// --------------------------------------------------------

async function bootstrap() {
  // app.use(express.static(join(process.cwd(), "../public")));

  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
    new ExpressAdapter()
  );

  // ----------------------

  app.enableCors();
  app.use(cookieParser());
  // app.use(helmet());
  // app.use(csurf());
  // app.use(
  //   rateLimit({
  //     windowMs: 15 * 60 * 1000, // 15 minutes
  //     max: 1000, // limit each IP to 1000 requests per windowMs
  //   })
  // );
  // ----------------------

  const port = process.env.PORT || 3000;

  app
    .get(NextModule)
    .prepare()
    .then(() => {
      app.listen(port, "0.0.0.0", () => {
        console.log(`> Ready on ${port}`);
      });
    });
}

bootstrap();
