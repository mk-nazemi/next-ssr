import {
  Controller,
  Get,
  Post,
  Body,
  ValidationPipe,
  Request,
} from "@nestjs/common";
import { IncomingMessage } from "http";
// --------------------------------------------

import { AuthService } from "./auth.service";

// --------------------------------------------

import {
  AuthResponseDto,
  RefreshTokenDto,
  AuthCredentialsDto,
  AuthPhoneDto,
} from "./dto-model-enum/";

// const authCredentialsDto1 = {
//   username: '09122584576',
//   password: '1234',
//   ApplicationType: 3,
// };

// --------------------------------------------

@Controller("/api/v1/auth")

// --------------------------------------------
export class AuthController {
  // ------------------------------

  constructor(private authService: AuthService) {}

  // ------------------------------

  // get app config for consume in front end!
  @Get("/config")
  getConfigOfApp(): Promise<{ [key: string]: string }> {
    return this.authService.getConfigOfApp();
  }
  // ------------------------------

  // check is token valid with response 200 or 401;
  @Get("/checkonline")
  checkStatus(@Request() req: IncomingMessage): any {
    return this.authService.checkStatus(req.headers);
  }

  // ------------------------------

  // we check user is valid or check token is already expired or not
  @Post("/refreshToken")
  checkRefreshToken(
    @Body(ValidationPipe) refreshToken: RefreshTokenDto
  ): Promise<AuthResponseDto> {
    return this.authService.checkRefreshToken(refreshToken);
  }
  // ------------------------------

  // for user sign in in application with user, pass ,and app type
  @Post("/signin")
  signIn(
    @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto
  ): Promise<AuthResponseDto> {
    return this.authService.signIn(authCredentialsDto);
  }

  // for user sign up in application with user, pass ,and app type
  @Post("/signin-otp")
  signUp(@Body(ValidationPipe) authPhoneDto: AuthPhoneDto): Promise<void> {
    return this.authService.signInOTP(authPhoneDto);
  }

  // ------------------------------
}
