export class AppConfig {
  AuthenticationConfuguration: {
    Mode: string;
    Applicationtype: string;
    tokenUrl: string;
    OTPUrl: string;
    ChangePasswordUrl: string;
    SendVerifyCodeUrl: string;
    VerifyCodeUrl: string;
    RegisterUrl: string;
    ComponentName: string;
    MultipleAccessItem: boolean;
    MultipleAccessItemUrl: string;
  };
}
