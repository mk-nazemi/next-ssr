import {
  IsString,
  IsNumber,
  IsBoolean,
  IsObject,
  IsOptional,
} from 'class-validator';

// ------------------------------

class SignInResultDto {
  @IsBoolean()
  Succeeded: boolean;

  @IsBoolean()
  IsLockedOut: boolean;

  @IsBoolean()
  IsNotAllowed: boolean;

  @IsBoolean()
  RequiresTwoFactor: boolean;
}

// ------------------------------

export class AuthResponseDto {
  @IsString()
  Access_token: string;

  @IsString()
  RefreshToken: string;

  @IsBoolean()
  IsVerify: boolean;

  @IsOptional()
  @IsObject()
  SignInResult: SignInResultDto;

  @IsNumber()
  Type: number;

  @IsOptional()
  @IsString()
  Message: string;

  @IsBoolean()
  CanUseCrm: boolean;
}
