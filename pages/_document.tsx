import Document, { Main, NextScript, Html, Head } from "next/document";
export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <head>
            <meta httpEquiv="content-type" content="text/html;charset=UTF-8" />
            <meta charSet="utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <meta
              httpEquiv="Content-Security-Policy"
              content="default-src *; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval' http://www.google.com"
            ></meta>
            {/*css guyz here*/}
          </head>
        </Head>
        <body>
          <Main />
          <NextScript />
          {/*script here*/}
        </body>
      </Html>
    );
  }
}
