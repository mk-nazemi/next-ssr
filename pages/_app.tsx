// ------------------------------------------

import React from "react";
import App, { Container, AppProps } from "next/app";
import Head from "next/head";
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";

// ------------------------------------------

import makeStore from "../lib/store";
import withReduxStore from "utils/with-redux-store";
import { appWithTranslation } from "utils/with-i18next";

// ------------------------------------------

// The Component prop is the active page,
// pageProps is an object with the initial props that were preloaded
// for your page by one of our data fetching methods,

// export default function MyApp({ Component, pageProps }: AppProps) {
//   return <Component {...pageProps} />;
// }

import "typeface-metropolis";
import "@typefaces-pack/typeface-inter";

interface AppPropsWithRedux extends AppProps {
  reduxStore: any;
}

class MyApp extends App {
  static async getInitialProps({ Component, ctx }: AppPropsWithRedux) {
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};
    return { pageProps };
  }

  render() {
    const { Component, pageProps, reduxStore } = this.props;
    return (
      <React.StrictMode>
        <Head>
          <title>React Next Boilerplate</title>
        </Head>

        <Provider store={reduxStore}>
          <Component {...pageProps} />
        </Provider>
      </React.StrictMode>
    );
  }
}

export default appWithTranslation(withReduxStore(MyApp));
